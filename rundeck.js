
class Script {
  process_incoming_request({ request }) {
    var alertColor = 'warning';

    if (request.content.status == 'succeeded') {
      alertColor = 'good';
    } else if (request.content.status == 'failed') {
      alertColor = 'danger';
    }

    let finFields = [];
    let status = 'unknown';
    let title;
    if (request.content.execution) {
        var exec = request.content.execution;
        status = exec.status;
        finFields.push({
          title: 'JobID',
          value: `[${exec.id}](${exec.href})`,
          short: true,
        });

        finFields.push({
          title: 'Usuário',
          value: `${exec.user}`,
          short: true,
        });

        finFields.push({
          title: 'Projeto',
          value: exec.project,
          short: true,
        });

        finFields.push({
          title: 'Job',
          value: `[${exec.job.name}](${exec.job.permalink})`,
          short: true,
        });

        finFields.push({
          title: 'Tipo de Execução',
          value: `${exec.executionType}`,
          short: true,
        });

        title = `${exec.project} / ${exec.job.name} / ${exec.id}`;
    }

    return {
      content: {
        username: 'Rundeck',
        text: `Job finalizou com status *${status}*`,
        attachments: [
          {
            color: alertColor,
            title_link: request.content.execution.href,
            title,
            fields: finFields,
          },
        ],
      },
    };
  }
}
