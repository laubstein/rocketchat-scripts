class Script {
  process_incoming_request({
    request
  }) {
    var alertColor = "warning";
    var alertKind  = "UNDEFINED";

    if (request.content.status == "resolved") {
      alertColor = "good";
      alertKind  = "RESOLVED"
    } else if (request.content.status == "firing") {
      alertColor = "danger";
      alertKind  = "PROBLEM"
    }
    console.log(request.content);

    let finFields = [];
    for (i = 0; i < request.content.alerts.length; i++) {
      var endVal = request.content.alerts[i];
      var elem = {
        title: alertKind + ": " + endVal.labels.alertname,
        value: "Source: " + request.content.receiver + "\n" + "Severity: " + endVal.labels.severity + "\n" + "Instance: " + endVal.labels.instance + "\n" + "Message: " + endVal.annotations.message + "\n" + "Description: " + endVal.annotations.description + "\n " + "Summary: " + endVal.annotations.summary,
        short: false
      };

      finFields.push(elem);
    }
    return {
      content: {
        username: "Alertmanager",
        attachments: [{
          color: alertColor,
          title_link: request.content.externalURL,
          title: "Prometheus notification",
          fields: finFields,
        }]
      }
    };

    return {
      error: {
        success: false,
        message: 'Error accepting Web Hook'
      }
    };
  }
}
