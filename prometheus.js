class Script {
  process_incoming_request({ request }) {
    var alertColor = 'warning';

    if (request.content.status == 'resolved') {
      alertColor = 'good';
    } else if (request.content.status == 'firing') {
      alertColor = 'danger';
    }

    let finFields = [];
    if (request.content.alerts) {
      for (var i = 0; i < request.content.alerts.length; i++) {
        var endVal = request.content.alerts[i];
        var elem = {
          title: 'alertname: ' + endVal.labels.alertname,
          value: '*instance:* ' + endVal.labels.instance,
          short: false,
        };

        finFields.push(elem);
        finFields.push({ title: 'descrição', value: endVal.annotations.description + '—' });
        finFields.push({ title: 'sumário', value: endVal.annotations.summary });
      }
    } else {
      if (request.content.attachments && request.content.attachments.length > 0) {
        // insert title if absent
        for (var idx = 0; idx < request.content.attachments.length; idx++) {
          if (!request.content.attachments[idx]['title']) {
            request.content.attachments[idx]['title'] = "";
          }
        }
        return request;
      }

      if (request.content.text && request.content.text.length > 0) {
        return request;
      }

      finFields.push({ title: 'descrição', value: 'teste de webhook' + '—' });
      finFields.push({ title: 'sumário', value: 'testado com sucesso!' });
    }

    return {
      content: {
        username: 'Alert Manager',
        attachments: [
          {
            color: alertColor,
            title_link: request.content.externalURL,
            title: 'Notificação do Prometheus',
            fields: finFields,
          },
        ],
      },
    };
  }
}
